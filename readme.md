# Acrylic word clock

![a word clock](images/wordclock_syncopate-800x600.jpg)


This is a clock that tell time in french quite similar in spirit to [Gouge](https://gitlab.com/avernois/gouge). 
It's made from laser cutted wood and acrylic, an esp8266, 24 apa102 leds and some other components.


The project is in a very early stage. Components and pcb are the same currently used in [Gouge](https://gitlab.com/avernois/gouge) but with 24 leds strip (instead of 64).

## code

Code can be found in [arduino/](arduino/).
It uses the arduino framework for ESP8266. Build and dependencies are managed with [plateform.io](https://platformio.org/).

## configuration
### wifi credentials

On the first start, a captive wifi portal will start.
Connect to that network (no credentials required) you should be redirected to a page where you can enter wifi credential for your own local network.
It should then connect to your network (and shutdown the captive network).

Wifi is used to synchronize the clock using ntp.

## Next

* let user configure timezone offset
* let user configure the color pattern
* warn user if syncing was not done since a long time
* manage dst automatically
* let user configure ntp pool