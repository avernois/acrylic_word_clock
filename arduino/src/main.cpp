#include <NTPClient.h> 
#include <WiFiUdp.h>

#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino

#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>         //https://github.com/tzapu/WiFiManager

#include "WordClockDisplay.h"

WiFiUDP ntpUDP;

NTPClient timeClient(ntpUDP, "europe.pool.ntp.org", 3600 *2, 60*60*1000);


WordClockDisplay *display;

void setup() {

    Serial.begin(115200);

    display = new WordClockDisplay();

    WiFiManager wifiManager;
    wifiManager.autoConnect("WordClock");
    Serial.println("connected...yeey :)");

    timeClient.begin();
}

void loop() {
    timeClient.update();
    
    display->display(timeClient.getHours(), timeClient.getMinutes());

    delay(200);
}
