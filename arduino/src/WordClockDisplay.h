#ifndef __have__WordClockDisplay_h__
#define __have__WordClockDisplay_h__

#include <stdint.h>

class WordClockDisplay {
  public:
  WordClockDisplay();
  void display(const int hour, const int minute);
};

#endif